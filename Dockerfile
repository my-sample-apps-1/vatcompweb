# syntax=docker/dockerfile:1

FROM mcr.microsoft.com/dotnet/aspnet:6.0.19

EXPOSE 80
ENV ASPNETCORE_URLS=http://+:80

WORKDIR /app
COPY . .

CMD ["dotnet", "VatCompWeb.dll"]
